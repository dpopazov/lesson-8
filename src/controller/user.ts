import { Context } from 'koa';
import { SESSION_KEY_COOKIE } from "../constants";
import { Session } from "../middlewares/sessionCountMiddleware";
import { encodeCookie } from "../helpers/cookiesHelpers";

export interface User {
  name: string;
  id: number;
  password: string;
}

const userArray: Array<User> = [
  {id: 1, name: 'Dima', password: '123'}
]

export function loginHandle(ctx: Context): void {
  const user = ctx.request.body as unknown as User;

  const existedUser = userArray.find(({id}) => id === user.id);
  if (existedUser && existedUser.password === user.password) {
    const userId = user.id;
    const counter = 0;
    const expires = new Date();

    const session: Session = { userId, counter, expires };

    ctx.cookies.set(
      SESSION_KEY_COOKIE,
      encodeCookie(session),
      { httpOnly: true }
    );

    ctx.body = { registered: true };
  } else {
    ctx.body = { registered: false };
    ctx.status = 404;
  }
}

export function getUsers(ctx: Context): void {
  const session: Session = ctx.state.session;

  if (session) {
    const updatedSession: Session = { ...session, counter: session.counter + 1 };

    ctx.cookies.set(
        SESSION_KEY_COOKIE,
        encodeCookie(updatedSession),
        { httpOnly: true }
    );
  }

  ctx.body = userArray;
}
