import * as Router  from 'koa-router';
import { getUsers, loginHandle } from '../controller/user';

const router = new Router({ prefix: '/user' });


router.get('/', getUsers);

router.post('/login', loginHandle);

export default router;
