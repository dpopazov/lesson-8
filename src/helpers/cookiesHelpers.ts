
export function encodeCookie(cookie: object): string {
    return new Buffer(JSON.stringify(cookie)).toString('base64');
}

export function decodeCookie(cookie: string): any {
    return JSON.parse(new Buffer(cookie, 'base64').toString('ascii'));
}
