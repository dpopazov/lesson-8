import * as Koa from 'koa';


const app = new Koa();
import userRouter from './router/user';
import * as bodyParser from 'koa-bodyparser';
import { sessionCountMiddleware } from "./middlewares/sessionCountMiddleware";
import { requestLoggerMiddleware } from "./middlewares/requestLoggerMiddleware";

const APP_PORT = process.env.PORT;
app.use(bodyParser());

app.use(sessionCountMiddleware);

app.use(requestLoggerMiddleware);

app.use(userRouter.routes());

app.listen(APP_PORT, () => {
  console.log('server is listening on port ', APP_PORT);
});
