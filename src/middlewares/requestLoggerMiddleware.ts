import { Context, Next } from 'koa';
import { Session } from "./sessionCountMiddleware";


export function requestLoggerMiddleware(ctx: Context, next: Next) {
    const session: Session | undefined = ctx.state.session;

    if (session) {
        console.log(` ======= Current user made ${session.counter} requests per session ======== `);
    }

    next();
}
