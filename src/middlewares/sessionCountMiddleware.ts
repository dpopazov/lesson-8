import { Context, Next } from 'koa';
import { SESSION_KEY_COOKIE } from "../constants";
import { decodeCookie } from "../helpers/cookiesHelpers";

export interface Session {
    userId: number;
    counter: number;
    expires: Date;
}


export function sessionCountMiddleware(ctx: Context, next: Next) {
    const cookie: string | undefined = ctx.cookies.get(SESSION_KEY_COOKIE);

    if (cookie) {
        ctx.state.session = decodeCookie(cookie);
    }

    next();
}
